<?php

/**
 * ProcessWire Configuration File
 *
 * Site-specific configuration for ProcessWire
 *
 * Please see the file /wire/config.php which contains all configuration options you may
 * specify here. Simply copy any of the configuration options from that file and paste
 * them into this file in order to modify them.
 *
 * ProcessWire 2.x
 * Copyright (C) 2014 by Ryan Cramer
 * Licensed under GNU/GPL v2, see LICENSE.TXT
 *
 * http://processwire.com
 *
 */

if(!defined("PROCESSWIRE")) die();

/*** SITE CONFIG *************************************************************************/

/**
 * Enable debug mode?
 *
 * Debug mode causes additional info to appear for use during dev and debugging.
 * This is almost always recommended for sites in development. However, you should
 * always have this disabled for live/production sites.
 *
 * @var bool
 *
 */
$config->debug = false;


/*** INSTALLER CONFIG ********************************************************************/


/**
 * Installer: Database Configuration
 * 
 */

// LIVE //
//////////
if ($_SERVER['HTTP_HOST'] == 'SITENAME.COM') {
    // staging or dev server
    $config->dbHost = 'HOSTNAME';
    $config->dbName = 'DB_NAME';
    $config->dbUser = 'DB_USER';
    $config->dbPass = 'DB_PASS';
    $config->dbPort = '3306';
    $config->isDev = false;
    $config->isLocal = false;
    $config->debug = false;
}

// STAGING //
/////////////
else if ($_SERVER['HTTP_HOST'] == '*.elwyn.co') {
    $config->dbHost = 'HOSTNAME';
    $config->dbName = 'DB_NAME';
    $config->dbUser = 'DB_USER';
    $config->dbPass = 'DB_PASS';
    $config->dbPort = '3306';
    $config->isDev = true;
    $config->isLocal = false;
    $config->debug = true;
}

// LOCAL //
///////////
else {
    $config->dbHost = 'localhost';
    $config->dbName = 'DB_NAME';
    $config->dbUser = 'root';
    $config->dbPass = 'moot';
    $config->dbPort = '3306';
    $config->isDev = true;
    $config->isLocal = true;
    $config->debug = true;
}

/**
 * Installer: User Authentication Salt 
 * 
 * Must be retained if you migrate your site from one server to another
 * 
 */
$config->userAuthSalt = '749729dae32e84a6956aba067f562bba'; 

/**
 * Installer: File Permission Configuration
 * 
 */
$config->chmodDir = '0755'; // permission for directories created by ProcessWire
$config->chmodFile = '0644'; // permission for files created by ProcessWire 

/**
 * Installer: Time zone setting
 * 
 */
$config->timezone = 'Europe/London';


/**
 * Installer: HTTP Hosts Whitelist
 * 
 */
$config->httpHosts = array('pw.dev');

