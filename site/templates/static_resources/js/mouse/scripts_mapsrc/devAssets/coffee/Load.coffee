Load = do ->

	# LOAD A GALLERY PAGE
	three = ( promise ) ->
		if window.isDev
			threePath = window.assetPath + '/devPlugins/three.min.js'
		else
			threePath = '//cdnjs.cloudflare.com/ajax/libs/three.js/85/three.min.js'
		loadjs threePath, {
			success: ->
				promise.resolve( true )
		}

	vendorScripts = ( useWebgl, promise ) ->
		scripts = []
		if useWebgl
			scripts.push window.assetPath + '/vendor/GPUParticleSystem.js'

		loadjs scripts, {
			success: ->
				promise.resolve()
		}

	data = ( path, promise ) ->
		$.getJSON path, (data) ->
			promise.resolve( data )


	{
		three: three
		vendorScripts: vendorScripts
	}