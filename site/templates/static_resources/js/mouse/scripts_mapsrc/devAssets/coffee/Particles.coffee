Particles = do ->
	
	camera = undefined
	tick = 0
	scene = undefined
	renderer = undefined
	clock = undefined
	controls = undefined
	container = undefined
	gui = undefined
	options = undefined
	spawnerOptions = undefined
	particleSystem = undefined

	win = {}
	mouse = {}

	init = ->
		setup()
		animate()

	setup = ->

		console.log 'setup partilces'

		clock = new (THREE.Clock)
		
		win = {
			w: window.innerWidth
			h: window.innerHeight
		}

		mouse = {
			x: win.w / 2
			y: win.h / 2
			px: 0
			py: 0
		}

		container = document.getElementById('particles')
		camera = new (THREE.PerspectiveCamera)(28, win.w / win.h, 1, 10000)
		camera.position.z = 100
		scene = new (THREE.Scene)
		
		# The GPU Particle system extends THREE.Object3D, and so you can use it
		# as you would any other scene graph component.	Particle positions will be
		# relative to the position of the particle system, but you will probably only need one
		# system for your whole scene
		particleSystem = new (THREE.GPUParticleSystem)(maxParticles: 250000)
		scene.add particleSystem
		
		# options passed during each spawned
		options =
			position: new (THREE.Vector3)
			positionRandomness: 50
			velocity: new THREE.Vector3(1,1,0),
			velocityRandomness: 0
			color: 0xFFF1E2
			colorRandomness: 0.5
			turbulence: 0.1
			lifetime: 4
			size: 20
			sizeRandomness: 1
			maxVelocity: 1

		spawnerOptions =
			spawnRate: 200
			horizontalSpeed: 1.5
			verticalSpeed: 1.33
			timeScale: 1
		
		# setup GUI
		if window.isDev
			do setupGUI

		# RENDERER
		renderer = new THREE.WebGLRenderer({
			antialias: true
			alpha: true
		})
		renderer.setPixelRatio window.devicePixelRatio
		renderer.setSize win.w, win.h
		renderer.setClearColor 0x000000, 0
		container.appendChild renderer.domElement
		
		window.addEventListener 'resize', onWindowResize, false
		window.addEventListener 'mousemove', onMouseMove, false



	onWindowResize = ( initial ) ->
		win = {
			w: window.innerWidth
			h: window.innerHeight
		}
		camera.aspect = win.w / win.h
		camera.updateProjectionMatrix()
		renderer.setSize win.w, win.h
	
	onMouseMove = ( e ) ->

		mouse =
			x: e.clientX
			y: e.clientY
		
		mouse.x = mouse.x - (win.w / 2)
		mouse.y = mouse.y - (win.h / 2)

		mouse.px = mouse.x / (win.w / 2)
		mouse.py = mouse.y / (win.h / 2)

		# console.log mouse


	setupGUI = ->
		gui = new (dat.GUI)(width: 350)
		gui.add(options, "velocityRandomness", 0, 30)
		gui.add(options, "positionRandomness", 0, 200)
		gui.add(options, "size", 20, 300)
		gui.add(options, "sizeRandomness", 0, 25)
		gui.add(options, "colorRandomness", 0, 1)
		gui.add(options, "lifetime", .1, 10)
		gui.add(options, "turbulence", 0, 1)
		gui.add(options, "maxVelocity", 0, 20)
		gui.add(spawnerOptions, "spawnRate", 10, 200)
		# gui.add(spawnerOptions, "horizontalSpeed", -20, 20)
		# gui.add(spawnerOptions, "verticalSpeed", -20, 20)
		gui.add(spawnerOptions, "timeScale", -1, 1)


	animate = ->
		requestAnimationFrame animate
		
		delta = clock.getDelta() * spawnerOptions.timeScale
		tick += delta
		if tick < 0
		  tick = 0
		if delta > 0
			# options.position.x = Math.sin(tick * spawnerOptions.horizontalSpeed) * 20
			# options.position.y = Math.sin(tick * spawnerOptions.verticalSpeed) * 10
			# options.position.z = Math.sin(tick * spawnerOptions.horizontalSpeed + spawnerOptions.verticalSpeed) * 5
			options.position.x = 0
			options.position.y = 0
			options.position.z = 0

			options.velocity.x = options.maxVelocity * mouse.px
			options.velocity.y = options.maxVelocity * -mouse.py
			options.velocity.z = 0

			x = 0
			while x < spawnerOptions.spawnRate * delta
				# Yep, that's really it.	Spawning particles is super cheap, and once you spawn them, the rest of
				# their lifecycle is handled entirely on the GPU, driven by a time uniform updated below
				particleSystem.spawnParticle options
				x++

		particleSystem.update tick
		render()
		return

	render = ->
		renderer.render scene, camera


	{
		init: init
	}