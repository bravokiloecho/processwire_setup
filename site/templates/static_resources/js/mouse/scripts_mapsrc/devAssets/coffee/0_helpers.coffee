Elwyn = do ->

	defineDeviceVariables = ->

		deviceVariables =
			isMobile: if $('#mobileTester').css('visibility') == 'visible' then true else false
			isTablet: if $('#tabletTester').css('visibility') == 'visible' then true else false
			isTouch: if Modernizr.touchevents then true else false

		deviceVariables.isDevice = if deviceVariables.isMobile or deviceVariables.isTablet then true else false

		return deviceVariables
	
	# Returns a random float or integer between min (inclusive) and max (inclusive)
	getRandomNumber = ( min, max, integer ) ->	
		if integer == false
			return Math.random() * ( max - min ) + min
		
		return Math.floor(Math.random() * (max - min + 1)) + min


	shuffleArray = ( array ) ->
		i = 0
		while i < array.length - 1
			j = i + Math.floor(Math.random() * (array.length - i))
			temp = array[j]
			array[j] = array[i]
			array[i] = temp
			i++
		return array


	# SWAP SVGs with PNGs
	svgFallback = (svgSupport) ->
		if !svgSupport
			$('img[src*="svg"]').attr 'src', ->
				$(this).attr('src').replace('.svg', '.png')

	randomString = (length, chars) ->
		mask = ""
		mask += "abcdefghijklmnopqrstuvwxyz"  if chars.indexOf("a") > -1
		mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"  if chars.indexOf("A") > -1
		mask += "0123456789"  if chars.indexOf("#") > -1
		mask += "~`!@#$%^&*()_+-={}[]:\";'<>?,./|\\"  if chars.indexOf("!") > -1
		result = ""
		i = length

		while i > 0
			result += mask[Math.round(Math.random() * (mask.length - 1))]
			--i

		return result

	placeholder = ( els, activeClass = "active" ) ->
			$(els).each ->
				$el = $(this)
				$el.val $el.data().placeholder
				if $.trim($el.val()) is ""
					$el
						.val placeholder
						.removeClass activeClass
						.parent()
						.removeClass activeClass
				$el.focus(->
					placeholder = $el.data().placeholder
					console.log 'val: '+$el.val()
					console.log 'placeholder: '+placeholder
					if $el.val() is placeholder
						$el
							.val ""
							.addClass activeClass
							.parent()
							.addClass activeClass
				).blur ->
					placeholder = $el.data().placeholder
					if $.trim($el.val()) is ""
						$el
							.val placeholder
							.removeClass activeClass
							.parent()
							.removeClass activeClass

	trackEvent = (category,action,label = 'undefined',value = 0) ->
		
		if typeof ga == 'undefined'
			# console.log 'Track event: '
			# console.log ' category: '+category
			# console.log ' action: '+action
			# console.log ' label: '+label
			return

		ga(
			'send',
			'event',
			category,
			action,
			label,
			value
		)

	preloadImage = ( src ) ->
		img = new Image()
		img.src = src


	getParameter = ( name ) ->
		results = new RegExp('[?&]' + name + '=([^&#]*)').exec(window.location.href)
		if results == null
			return false
		else
			return results[1] or 0

	
	# http://stackoverflow.com/questions/5899783/detect-safari-chrome-ie-firefox-opera-with-user-agent
	browserDetect = ->
		browser = 
			chrome: navigator.userAgent.indexOf('Chrome') > -1
			explorer: navigator.userAgent.indexOf('MSIE') > -1
			firefox: navigator.userAgent.indexOf('Firefox') > -1
			safari: navigator.userAgent.indexOf("Safari") > -1
			opera: navigator.userAgent.toLowerCase().indexOf("op") > -1
		
		if browser.chrome and browser.safari
			browser.safari = false
		if browser.chrome and browser.opera 
			browser.chrome = false

		return browser


	# plugin for selecting text
	# from: http://is.gd/E1kgQ7
	$.fn.selectText = ->
		doc = document
		element = this[0]
		range = undefined
		selection = undefined
		if doc.body.createTextRange
			range = document.body.createTextRange()
			range.moveToElementText element
			range.select()
		else if window.getSelection
			selection = window.getSelection()
			range = document.createRange()
			range.selectNodeContents element
			selection.removeAllRanges()
			selection.addRange range

	# plugin for setting opacity
	$.fn.opacity = ( o ) ->
		@each ->
			$(this).css opacity: o

	# Degrees to radians
	degToRad = ( deg ) ->
		return deg * (Math.PI / 180)

	# Radians to degress
	radToDeg = ( rad ) ->
		return rad / (Math.PI / 180)

	stripTrailingCharacter = ( string, char ) ->
		if string.substr( string.length - 1 ) == char
			return string.substr(0, string.length - 1)
		return string

	stripLeadingCharacter = ( string, char ) ->
		if string[0] == char
			return string.substr(1)
		return string

	stripTrailingSlash = ( url ) ->
		if not url
			return

		return stripTrailingCharacter url, '/'


	# Pad numbers, where...
	# 'n' is the number to be padded
	# 'width' is the desired width of the new number
	# 'z' is the character to padd with. Defaults to 0
	# See: http://stackoverflow.com/a/10073788/993297
	pad = (n, width, z) ->
		z = z or '0'
		n = n + ''
		if n.length >= width then n else new Array(width - (n.length) + 1).join(z) + n

	logJson = ( json ) ->
		console.log JSON.stringify json, null, '\t'
	


	# EXPORT
	{
		svgFallback: svgFallback
		randomString: randomString
		shuffleArray: shuffleArray
		placeholder: placeholder
		trackEvent: trackEvent
		getRandomNumber: getRandomNumber
		preloadImage: preloadImage
		getParameter: getParameter
		defineDeviceVariables: defineDeviceVariables
		browserDetect: browserDetect
		degToRad: degToRad
		radToDeg: radToDeg
		stripLeadingCharacter: stripLeadingCharacter
		stripTrailingCharacter: stripTrailingCharacter
		stripTrailingSlash: stripTrailingSlash
		pad: pad
		logJson: logJson
	}