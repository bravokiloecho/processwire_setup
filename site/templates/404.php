<? // PAGE VARS
    $isAjax = $config->ajax;
    $currentPage = '404';
?>

<?
    // Processwire global variables
    include('./includes/globalVariables.inc');
    
    if(!$isAjax) {
        include("./includes/head.inc");
        echo "<script>var currentPage = window.currentPage = '{$currentPage}';</script>";
    }
?>

<div id="wrapper" class="<?=$currentPage?>" data-page="<?=$currentPage?>">

    <?
        if(!$isAjax) {
            include("./globalElements/header.php");
            include("./globalElements/siteLinks.php");
        }
    ?>
    
    <main id="pjax-wrapper" class="main-content page-content" role="main">
        <div
            class="pjax-container"
            data-namespace="<?=$currentPage?>"
            data-title="<?=$page_title?>"
            data-slug="<?=$page->name?>"
        >
            
            <h2 class="home-page-intro 404-page-text">
                <?=$page->text_block?>
            </h2>

        </div>
    </main>

    <?
        if(!$isAjax) {
            include("./globalElements/footer.php");
        }
    ?>

</div> <? //end of #wrapper ?>


<?
    if(!$isAjax) {
        include("./includes/foot.inc");
    }
?>