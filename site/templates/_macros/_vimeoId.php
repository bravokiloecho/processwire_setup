<?php
	
	/* *** USAGE ***
	$youtubeId = wireRenderFile("_macros/_youtubeId", array('url' => $youtubeUrl ));
	*/

	// Taken from: https://github.com/lingtalfi/video-ids-and-thumbnails/blob/master/function.video.php


	if (preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
		return $m[1];
	} else {
	    return false;
	}

?>