<?php
	
	/* *** USAGE ***
	
	$leadImageCaption = wireRenderFile("_macros/_imageCaption", array(
		'image' => $leadImage
	));
	
	*/

	$caption = $image->description ? $image->description : $image->name;
	echo $caption;

?>