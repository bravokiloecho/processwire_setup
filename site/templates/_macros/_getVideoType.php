<?php
	/* README
	Identifies whether a video is a YouTube or Vimeo video based on its URL
	*/

	/* *** USAGE ***
	$embed = wireRenderFile("_macros/_getVideoType", array('url' => $videoLink ));
	*/

	if(strpos($url, '://www.youtube.com/') !== false || strpos($url, '://youtu.be/') !== false) {
		echo 'youtube';
	} elseif(strpos($url, '://vimeo.com/') !== false) {
		echo 'vimeo';
	} else {
		echo 'unknown';
	}

?>

