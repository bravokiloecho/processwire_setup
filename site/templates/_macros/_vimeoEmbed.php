<?php
	
	/* *** USAGE ***
	$youtubeEmbed = wireRenderFile("_macros/_youtubeEmbed", array('url' => $youtubeUrl ));
	*/

	$videoId = wireRenderFile('_macros/_vimeoId', array(
		'url' => $url
	));
?>

<div class="vimeo-iframe-wrapper">
	<iframe class="vimeo-iframe" src="https://player.vimeo.com/video/<?=$videoId?>?title=0&byline=0&portrait=0" width="640" height="267" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>