<?php

	// Returns url src

	/* ** USAGE **
	
	$imageSrc = wireRenderFile("_macros/_resizeImage", array(
		'image' => $image,
		'longEdge' => $longEdge,
		'width' => $width,
		'height' => $height,
		'cropping' => $cropping
	));
	
	*/
	
	$options = array(
		'upscaling' => true,
		'cropping' => isset($cropping) ? $cropping : false,
		'quality' => 90,
		'sharpening' => 'none' 
	);

	$img = '';
	$src = '';
	$width = isset($width) ? $width : false;
	if ( $width == 'auto' ) {
		$width = false;
	}
	$height = isset($height) ? $height : false;
	if ( $height == 'auto' ) {
		$height = false;
	}

	if ( $width && $height ) {
		$img = $image->size( $width, $height, $options );
		$src = $img->url;

	}
	else if ( $width ) {
		$img = $image->width( $width, $options );
		$src = $img->url;

	} else if ( $height ) {

		$img = $image->height( $height, $options );
		$src = $img->url;

	} else {

		if ( $image->width > $image->height ) {
			$img = $image->width( $longEdge, $options );
		} else {
			$img = $image->height( $longEdge, $options );
		}

	}

	$src = $img->url;

	// If we want all the props, return an array
	if ( isset($return) && $return == 'props' ) {
		$width = $img->width();
		$height = $img->height();
		echo $src . '|*|' . $width . '|*|' . $height;
	} else {
		echo $src;
	}

?>