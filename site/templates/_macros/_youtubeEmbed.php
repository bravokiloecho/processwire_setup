<?php
	
	/* *** USAGE ***
	$youtubeEmbed = wireRenderFile("_macros/_youtubeEmbed", array('url' => $youtubeUrl ));
	*/

	$videoId = wireRenderFile('_macros/_youtubeId', array(
		'url' => $url
	));
?>

<div class="youtube-iframe-wrapper">
	<iframe class="youtube-iframe" width="560" height="315" src="https://www.youtube.com/embed/<?=$videoId?>" frameborder="0" allowfullscreen></iframe>
</div>