<?php
	
	/* *** USAGE ***
	$videoEmbed = wireRenderFile("_macros/_videoEmbed", array('videoLink' => $videoLink ));
	*/

	$videoType = wireRenderFile('_macros/_getVideoType', array(
		'url' => $videoLink
	));

	$videoId = '';
	if ( $videoType == 'youtube' ) {
		$videoId = wireRenderFile('_macros/_youtubeId', array(
			'url' => $videoLink
		));
	} else {
		$videoId = wireRenderFile('_macros/_vimeoId', array(
			'url' => $videoLink
		));
	}
?>

<div class="<?php echo $videoType ?>-iframe-wrapper">
	<?php if ( $videoType == 'youtube' ): ?>	
		<iframe class="youtube-iframe" width="560" height="315" src="https://www.youtube.com/embed/<?=$videoId?>" frameborder="0" allowfullscreen></iframe>
	<?php else: ?>
		<iframe class="vimeo-iframe" src="https://player.vimeo.com/video/<?=$videoId?>?title=0&byline=0&portrait=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	<?php endif ?>
</div>