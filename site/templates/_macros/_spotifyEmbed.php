<?php

	/* *** USAGE ***
	$embed = wireRenderFile("_macros/_spotifyEmbed", array('embedLink' => $embedLink ));
	*/

	/* README
	https://developer.spotify.com/technologies/widgets/spotify-play-button/

	PLAYER SIZE	HEIGHT	WIDTH
	Minimum size	80px	250px
	Maximum size	720px	640px

	*/
?>

<iframe class="spotify-embed" src="https://embed.spotify.com/?uri=<?=$embedLink?>&theme=white" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>