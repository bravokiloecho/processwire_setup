<div class="social-media-links">
	<?php if ( $g['twitterLink'] ): ?>
		<a href="<?php echo $g['twitterLink'] ?>" class="social-media-link icon-twitter" title="twitter" target="_blank" rel="noopener noreferrer"></a>
	<?php endif ?>
	<?php if ( $g['facebookLink'] ): ?>
		<a href="<?php echo $g['facebookLink'] ?>" class="social-media-link icon-facebook" title="facebook" target="_blank" rel="noopener noreferrer"></a>
	<?php endif ?>
	<?php if ( $g['instagramLink'] ): ?>
		<a href="<?php echo $g['instagramLink'] ?>" class="social-media-link icon-instagram" title="instagram" target="_blank" rel="noopener noreferrer"></a>
	<?php endif ?>
	<?php if ( $g['linkedinLink'] ): ?>
		<a href="<?php echo $g['linkedinLink'] ?>" class="social-media-link icon-linked" title="instagram" target="_blank" rel="noopener noreferrer"></a>
	<?php endif ?>
</div>