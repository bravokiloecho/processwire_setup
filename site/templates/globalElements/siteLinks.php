<nav id="site-links" class="site-links page-content">
	<div class="site-links-inner inner-content">
		<?php // Build the links dynamically
			$pages = $g['home']->children();
			$currentPageId = $page->id;
			foreach ( $pages as $pageLink ) {
				$pageUrl = $pageLink->url;
				$pageTitle = ($pageLink->topLinkText ? $pageLink->topLinkText : $pageLink->title);
				$pageId = $pageLink->id;
				$pageSlug = $pageLink->name;
				$linkClass = ($pageId == $currentPageId ? ' active' : '');
				$linkClass .= ' ' . $pageSlug;
		?>
			<a class="site-link<?=$linkClass?>" href="<?=$pageUrl?>">
				<span class="site-link-inner"><?=$pageTitle?></span>
			</a>

		<?php } // End of foreach loop?>
	</div>
</nav>