<?php // PAGE VARS
    $isAjax = $config->ajax;
    $currentPage = 'home';
?>

<?php
    // Processwire global variables
    include('./includes/globalVariables.inc');
    
    if( !$isAjax ) {
        include("./includes/siteFunctions.php");
        include("./includes/head.inc");
        echo "<script>var currentPage = window.currentPage = '{$currentPage}';</script>";
    }
?>

<div id="wrapper" class="<?=$currentPage?>" data-page="<?=$currentPage?>">

    <?php
        if( !$isAjax  ) {
				include("./globalElements/header.php");
        }
    ?>
    
    <main id="pjax-wrapper" class="main-content page-content" role="main">
        <div
            class="pjax-container"
            data-namespace="<?=$currentPage?>"
            data-title="<?=$g['page_title']?>"
            data-slug="<?=$page->name?>"
        >
            <!-- CONTENT -->
            <?php include("./pageElements/homeContent.php"); ?>
        </div>
    </main>

    <?php
        if( !$isAjax ) {
           include("./globalElements/footer.php");
        }
    ?>

</div> <?php //end of #wrapper ?>


<?php
    if( !$isAjax ) {
        include("./includes/foot.inc");
    }
?>