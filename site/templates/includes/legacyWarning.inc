<!--[if lt IE 9]>
<aside id="legacy-warning">
    <p class="message">This browser is not supported. Kindly consider using another:</p>
    <ul class="new-browser-list">
        <li><a href="https://www.google.com/intl/en/chrome/browser">chrome</a>
        </li>
        <li><a href="http://support.apple.com/en_GB/downloads/#safari">safari</a>
        </li>
        <li><a href="http://www.mozilla.org/en-US/firefox/new">firefox</a>
        </li>
    </ul>
</aside>
<![endif]-->