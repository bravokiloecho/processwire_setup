<?php    
    $twitterSiteUser = $page->twitter_handle;
    $twitterSiteCreator = '@elwynco';
    $smImage = false;
    $twitterCardType = 'summary';
    if ( $page->socialMediaImage ) {
    	$smImage = $page->socialMediaImage->width(800)->httpUrl;
    	$twitterCardType = $page->twitterCardType;
    } else if ( $g['home']->socialMediaImage ) {
    	$smImage = $g['home']->socialMediaImage->width(800)->httpUrl;
    	$twitterCardType = $g['home']->twitterCardType;
    }
?>

<!-- OG CARD -->
<meta property="og:site_name" content="<?=$g['site_title']?>" />
<meta property="og:title" content="<?=$g['page_title']?>" />
<meta property="og:description" content="<?=$g['meta_description']?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?=$page->httpUrl?>" />
<meta property="og:image" content="<?=$smImage?>" />
<!-- TWITTER CARD -->
<meta name="twitter:card" content="<?=$twitterCardType?>">
<meta name="twitter:site" content="<?=$twitterSiteUser?>">
<meta name="twitter:title" content="<?=$g['page_title']?>">
<meta name="twitter:description" content="<?=$g['meta_description']?>">
<meta name="twitter:image" content="<?=$smImage?>">