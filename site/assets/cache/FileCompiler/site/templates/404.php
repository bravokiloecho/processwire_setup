<? // PAGE VARS
    $isAjax = $config->ajax;
    $currentPage = '404';
?>

<?
    // Processwire global variables
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/includes/globalVariables.inc',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    
    if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/includes/head.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        echo "<script>var currentPage = window.currentPage = '{$currentPage}';</script>";
    }
?>

<div id="wrapper" class="<?=$currentPage?>" data-page="<?=$currentPage?>">

    <?
        if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/globalElements/header.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/globalElements/siteLinks.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        }
    ?>
    
    <main id="pjax-wrapper" class="main-content page-content" role="main">
        <div
            class="pjax-container"
            data-namespace="<?=$currentPage?>"
            data-title="<?=$page_title?>"
            data-slug="<?=$page->name?>"
        >
            
            <h2 class="home-page-intro 404-page-text">
                <?=$page->text_block?>
            </h2>

        </div>
    </main>

    <?
        if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/globalElements/footer.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        }
    ?>

</div> <? //end of #wrapper ?>


<?
    if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/includes/foot.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    }
?>