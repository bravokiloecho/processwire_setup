<?php // PAGE VARS
    $isAjax = $config->ajax;
    $currentPage = 'home';
?>

<?php
    // Processwire global variables
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/includes/globalVariables.inc',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    
    if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/includes/siteFunctions.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/includes/head.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        echo "<script>var currentPage = window.currentPage = '{$currentPage}';</script>";
    }
?>

<div id="wrapper" class="<?=$currentPage?>" data-page="<?=$currentPage?>">

    <?php
        if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/globalElements/header.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        }
    ?>
    
    <main id="pjax-wrapper" class="main-content page-content" role="main">
        <div
            class="pjax-container"
            data-namespace="<?=$currentPage?>"
            data-title="<?=$g['page_title']?>"
            data-slug="<?=$page->name?>"
        >
            <!-- CONTENT -->
            <?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/pageElements/homeContent.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
        </div>
    </main>

    <?php
        if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/globalElements/footer.php",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
        }
    ?>

</div> <?php //end of #wrapper ?>


<?php
    if(!$isAjax) {
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/includes/foot.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    }
?>