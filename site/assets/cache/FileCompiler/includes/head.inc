<!DOCTYPE html>
<!--[if IE 7]><html data-ie="ie7" lang="en"><![endif]-->
<!--[if IE 8]><html data-ie="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html data-ie="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<?php include("./elwyn_signature.inc"); ?>

<head>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<?php // VIEWPORT SETTINGS ?>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale = 1.0, maximum-scale = 1.0" id="meta-viewport">

<meta id="meta-description" name="description" content="<?php echo $g['meta_description']?>">
<meta name="keywords" content="<?php echo $g['meta_keywords']?>">
<!-- Very short sentence describing the purpose of the website -->
<meta name="abstract" content="<?php echo $g['home']->site_abstract?>">
<!-- Describes the topic of the website -->
<meta name="topic" content="<?php echo $g['home']->site_topic?>">
<!-- Brief summary of the company or purpose of the website -->
<meta name="summary" content="<?php echo $g['home']->site_summary?>">

<meta http-equiv="Content-Language" content="en">
<meta name="author" content="elwyn.co">
<?php // Makes sure your website shows up in all countries and languages ?>
<meta name="coverage" content="Worldwide">
<?php // Does the same as the coverage tag ?>
<meta name="distribution" content="Global">

<title id="meta-title"><?php echo $g['page_title']?></title>

<?php // if ( $live ) {
	include("./og_info.inc");
//} ?>

<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="msapplication-tap-highlight" content="no"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<?php 
	// META ICONS
	include("./metaIcons.inc");
?>

<?php // CSS ?>
<link type='text/css' href='<?php echo $g['assets']?>css/<?php echo $g['assetType']?>/index.css<?php echo $v?>' rel='stylesheet' media='all' />

<script>
	var assetPath = window.assetPath = '<?php echo $g['assets']?>';
</script>

<?php if ( $local || $dev ) { // LOCAL JS FILES ?>
	
	<!-- jQuery -->
	<script src='<?php echo $g['assets']?>jquery/jquery_3.js'></script>
	
	<!-- Plugins -->
	<script src='<?php echo $g['assets']?>js/vendor.js'></script>
	<script src='<?php echo $g['assets']?>js/browserify.js'></script>
	<!-- <script src='<?php echo $g['assets']?>devPlugins/dat.gui.min.js'></script> -->
	<script src='<?php echo $g['assets']?>js/mouse/scripts.js'></script>

	<script>var isDev = window.isDev = true;</script>

	<!-- BLOCK ROBOTS-->
	<meta name="robots" content="noindex">

<?php } else { // LIVE JS FILES PLUS GOOGLE ANALYTICS ?>

	<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo $g['assets']?>jquery/jquery_1.js"><\/script>')</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
	    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="<?php echo $g['assets']?>jquery/jquery_3.js"><\/script>')</script>
	<!--<![endif]-->

	<!-- FIX FOR IE MAIN -->
	<script>document.createElement('main');</script>

	<!-- HTML5 SHIV, if necessary -->
	<!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->

	<script src='<?php echo $g['assets']?>js/min/scripts.js<?php echo $v?>'></script>

	<script>var isDev = window.isDev = false;</script>

	<?php if ($local): ?>
		<script>var isDev = window.isLocal = true;</script>	
	<?php endif ?>

	<script>
		// GOOGLE ANALYTICS
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'XX-XXXXXXXX-X', 'auto');
		ga('send', 'pageview');
	</script>
<?php } ?>

</head>

<body class="<?php echo $currentPage?>">