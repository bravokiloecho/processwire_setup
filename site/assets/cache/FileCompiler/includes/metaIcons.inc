<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico<?=$v?>2"/>
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?=$g['gfx']?>meta/Icon-60@3x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?=$g['gfx']?>meta/Icon-76@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$g['gfx']?>meta/Icon-72@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=$g['gfx']?>meta/Icon-60@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?=$g['gfx']?>meta/Icon-Small-40@3x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$g['gfx']?>meta/Icon@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="100x100" href="<?=$g['gfx']?>meta/Icon-Small-50@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="87x87" href="<?=$g['gfx']?>meta/Icon-Small@3x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="80x80" href="<?=$g['gfx']?>meta/Icon-Small-40@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?=$g['gfx']?>meta/Icon-76.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$g['gfx']?>meta/Icon-72.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?=$g['gfx']?>meta/Icon-60.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="58x58" href="<?=$g['gfx']?>meta/Icon-Small@2x.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?=$g['gfx']?>meta/Icon.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="50x50" href="<?=$g['gfx']?>meta/Icon-Small-50.png<?=$v?>"/>
<link rel="apple-touch-icon-precomposed" sizes="29x29" href="<?=$g['gfx']?>meta/Icon-Small.png<?=$v?>"/>
<meta name="msapplication-TileColor" content="#ffffff"/>
<meta name="msapplication-TileImage" content="<?=$g['gfx']?>meta/Icon-72@2x.png<?=$v?>"/>