<?php
	
	$g = array();

	// DEFINE SOME GLOBAL VARIABLES
	$g['home'] = $pages->get('/');
	$g['root'] = $config->urls->root;
	$g['assets'] = '/site/templates/static_resources/';
	$g['gfx'] = $g['assets'] . 'gfx/';

	// SET GLOBAL IMAGE OPTIONS
	// https://processwire.com/api/fieldtypes/images/
	$g['imageOptions'] = array(
		'quality' => 85,
		'upscaling' => false
	);
	
	$g['emailAddress'] = $g['home']->email_address;
	$g['emailLink'] = '<a href="'.$g['emailAddress'].'">'.$g['emailAddress'].'</a>';
	$g['telephoneNumber'] = $g['home']->telephone_number;
	$g['address'] = $g['home']->land_address;
	$g['inlineAddress'] = str_replace("\n", ", ", $g['address']);
	$g['paragraphAddress'] = nl2br( $g['address'] );
	$g['googleMapLink'] = $g['home']->googleMapLink;
	$g['twitterHandle'] = $g['home']->twitter_handle;
	$g['twitterLink'] = 'https://twitter.com/' . $g['twitterHandle'];
	$g['instagramHandle'] = $g['home']->instagram_handle;
	$g['instagramLink'] = 'https://www.instagram.com/' . $g['instagramHandle'];
	$g['facebookLink'] = $g['home']->facebook_link;
	$g['vimeoLink'] = $g['home']->vimeoLink;

	$v = '?v' . date("Y-m-d",time());
	$local = $config->isLocal;
	$dev = $config->isDev;
	$live = $config->isLive;
	$devParam = false;

	if ( isset( $_GET['dev'] )) {
		$devParam = $_GET['dev'];
	}

	if ( $devParam ) {
		$dev = true;
	}

	if ($dev || $local) {
		$g['assetType'] = 'mouse';
	}
	else {
		$g['assetType'] = 'min';
	}


	// And add the current page's title if it exists
	// Or sitewide if this doesn't exist
	$g['site_title'] = $g['home']->site_title;
	$g['page_title'] = $g['site_title'];
	// If not home page...
	if ( $page->id != $g['home']->id ) {
		$g['currentPageTitle'] = $page->page_title ? $page->page_title : $page->title;
		$g['page_title'] = $g['home']->site_title . ' | ' . $g['currentPageTitle'];
	}

	// Set the meta description to current page description
	// Or sitewide if this doesn't exist
	$g['meta_description'] = '';
	if ( $page->page_description ) {
		$g['meta_description'] = $page->page_description;
	} else {
		$g['meta_description'] = $g['home']->site_description;
	}

	$g['meta_keywords'] = '';
	if ( $page->page_keywords ) {
		$g['meta_keywords'] = $page->page_keywords;
	} else {
		$g['meta_keywords'] = $g['home']->site_keywords;
	}

	// PAGES
	$g['aboutPage'] = $pages->findOne('template=about');
?>